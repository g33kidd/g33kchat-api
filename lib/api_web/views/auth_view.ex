defmodule ApiWeb.AuthView do
	use ApiWeb, :view

	def render("auth.json", %{token: token, user: user}) do
		%{token: token, 
			user: %{
				username: user.username,
				email: user.email,
				inserted_at: user.inserted_at,
				updated_at: user.updated_at
			}
		}
	end
end