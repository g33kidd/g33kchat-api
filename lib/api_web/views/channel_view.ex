defmodule ApiWeb.ChannelView do
  use ApiWeb, :view
  alias ApiWeb.ChannelView

  def render("index.json", %{channels: channels}) do
    render_many(channels, ChannelView, "channel.json")
  end

  def render("show.json", %{channel: channel}) do
    render_one(channel, ChannelView, "channel.json")
  end

  def render("channel.json", %{channel: channel}) do
    %{id: channel.id,
      identifier: channel.identifier,
      name: channel.name,
      description: channel.description,
      inserted_at: channel.inserted_at,
      updated_at: channel.updated_at}
  end
end
