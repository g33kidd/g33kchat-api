defmodule ApiWeb.ChannelChannel do
  use ApiWeb, :channel

  def join("channel:" <> identifier, payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  def handle_in("message:create", payload, socket) do
    resource = current_resource(socket)
    IO.inspect resource
    broadcast socket, "message:new", %{
      message: payload["message"],
      user: %{
        username: resource.username
      }
    }
    # broadcast socket, "message:new", %{
    #   payload: payload["message"],
    #   user: %{
    #     username: resource.username
    #   }
    # }
    {:noreply, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (channel:lobby).
  def handle_in("shout", payload, socket) do
    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
