defmodule ApiWeb.Plug.AuthPipeline do

	use Guardian.Plug.Pipeline,
		otp_app: :api,
		module: Api.Guardian,
		error_handler: ApiWeb.FallbackController

	plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}, realm: :none
	plug Guardian.Plug.EnsureAuthenticated
	plug Guardian.Plug.LoadResource, ensure: true
end