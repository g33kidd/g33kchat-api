defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ApiWeb do
    pipe_through :api

    # Authentication
    post "/signup", AuthController, :signup
    post "/auth", AuthController, :authenticate
    post "/verify", AuthController, :verify

    # Channels
    get "/channels", ChannelController, :index
    get "/channels/:channel", ChannelController, :show
    post "/channels", ChannelController, :create
    delete "/channels/:id", ChannelController, :destroy
  end
end
