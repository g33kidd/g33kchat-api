defmodule ApiWeb.ChannelController do
  use ApiWeb, :controller
  alias Api.Channels

  action_fallback ApiWeb.FallbackController
  plug ApiWeb.Plug.AuthPipeline when action in [:create, :destroy]

  def index(conn, _) do
    channels = Channels.list_channels()
    render conn, "index.json", channels: channels
  end

  def show(conn, %{"channel" => channel_id}) do
    channel = Channels.get_channel_by_identifier channel_id
    render conn, "show.json", channel: channel
  end

  def create(conn, %{"name" => name, "identifier" => identifier, "description" => description} = params) do
    user = current_resource(conn)
    channel_params = %{name: name, identifier: identifier, description: description}
    IO.inspect channel_params
    with channel <- Channels.create_channel(channel_params, user) do
      render conn, "show.json", channel: channel
    end
  end

  def destroy(conn, %{"id" => id}) do
    channel = Channels.get_channel(id)
    with :ok <- Channels.delete_channel(channel) do
      send_resp conn, 200, ""
    end
  end

end
