defmodule ApiWeb.AuthController do
	use ApiWeb, :controller
	alias Api.Accounts

	action_fallback ApiWeb.FallbackController
	plug ApiWeb.Plug.AuthPipeline when action in [:verify]

	def signup(conn, %{"username" => username, "email" => email, "password" => password}) do
		user_attrs = %{username: username,
									 password: password,
									 email: email}

		with {:ok, user} <- Accounts.create_user(user_attrs),
				 {:ok, token, claims} <- Api.Guardian.encode_and_sign(user) do
			render conn, "auth.json", %{token: token, user: user}
		end
	end

	def authenticate(conn, %{"username" => username, "password" => password}) do
		with {:ok, user} <- Accounts.verify_user(%{username: username}, password),
				 {:ok, token, claims} <- Api.Guardian.encode_and_sign(user) do
			render conn, "auth.json", %{token: token, user: user}
		end
	end

	def verify(conn, _) do
		resource = Api.Guardian.Plug.current_resource(conn)
		token = Api.Guardian.Plug.current_token(conn)
		render conn, "auth.json", %{token: token, user: resource}
	end
end