defmodule ApiWeb.FallbackController do
	use ApiWeb, :controller

	def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(Api.ErrorView, :"404")
  end

  def auth_error(conn, {type, reason}, _opts) do
    body = Poison.encode!(%{message: to_string(type)})
    send_resp(conn, 401, body)
  end

  # TODO: Add changeset view
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> render(Api.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, :unauthorized}) do
    conn |> put_status(:unauthorized)
  end

  def call(conn, {:error, :no_resource}) do
    conn
    |> put_status(:unauthorized)
  end
end