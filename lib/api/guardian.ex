defmodule Api.Guardian do
	use Guardian, otp_app: :api

	def subject_for_token(resource, _claims) do
    {:ok, "User:" <> to_string(resource.id)}
  end
  def subject_for_token(_, _) do
    {:error, :no_resource}
  end

  def resource_from_claims(%{"sub" => "User:" <> user_id}) do
    try do
      case Integer.parse(user_id) do
        {id, ""} -> {:ok, Api.Accounts.get_user!(user_id)}
        _ -> {:error, :no_resource}
      end
      rescue
        Ecto.NoResultsError -> {:error, :no_resource}
    end
  end
  def resource_from_claims(_claims) do
    {:error, :no_resource}
  end
end