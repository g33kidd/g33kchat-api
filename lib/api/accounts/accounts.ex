defmodule Api.Accounts do
	alias Api.Accounts.User
	alias Api.Repo

	def create_user(attrs) do
		%User{}
		|> User.changeset(attrs)
		|> Repo.insert()
	end

	def get_user(id), do: Repo.get(User, id)
	def get_user!(id), do: Repo.get!(User, id)

	def verify_user(attrs, password) do
		user = Repo.get_by(User, attrs)
		case Comeonin.Bcrypt.checkpw(password, user.hash) do
			true -> {:ok, user}
			false -> {:error, :unauthorized}
			_ -> {:error, :unknown}
		end
	end
end
