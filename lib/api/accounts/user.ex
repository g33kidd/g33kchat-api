defmodule Api.Accounts.User do
	use Ecto.Schema
	import Ecto.Changeset
	alias Api.Accounts.User

	schema "users" do
		field :username, :string
		field :email, :string
		field :hash, :string
		field :password, :string, virtual: true
		has_many :channels, Api.Channels.Channel

		timestamps()
	end

	def changeset(%User{} = user, attrs) do
		user
		|> cast(attrs, [:username, :email, :password])
		|> validate_required([:username, :email, :password])
		|> unique_constraint(:username)
		|> unique_constraint(:email)
		|> put_password_hash()
	end

	def put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
		change(changeset, Comeonin.Bcrypt.add_hash(password, [hash_key: :hash]))
	end
	def put_password_hash(changeset), do: changeset
end
