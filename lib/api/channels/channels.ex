defmodule Api.Channels do
  alias Api.Repo
  alias Api.Channels.Channel

  def list_channels, do: Repo.all(Channel)

  def get_channel(id), do: Repo.get(Channel, id)
  def get_channel!(id), do: Repo.get!(Channel, id)

  def get_channel_by_identifier(id) do
    Repo.get_by(Channel, identifier: id)
  end

  def create_channel(attrs, user) do
    Ecto.build_assoc(user, :channels, attrs)
    |> Repo.insert!()
  end

  def delete_channel(%Channel{} = channel), do: Repo.delete(channel)
end
