defmodule Api.Channels.Channel do
  use Ecto.Schema
  import Ecto.Changeset
  alias Api.Channels.Channel


  schema "channels" do
    field :description, :string
    field :identifier, :string
    field :name, :string
    belongs_to :user, Api.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(%Channel{} = channel, attrs) do
    channel
    |> cast(attrs, [:name, :identifier, :description])
    |> validate_required([:name, :identifier, :description])
    |> unique_constraint(:identifier)
  end
end
