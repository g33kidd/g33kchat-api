# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :api,
  ecto_repos: [Api.Repo]

# Configures the endpoint
config :api, ApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8LN4wskGLMNbWl5KCcAkmAhU6HRXvynr27kn6KJwkOidE94M2X4CE6vwCid679e4",
  render_errors: [view: ApiWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Api.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :bcrypt_elixir, :log_rounds, 4

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :api, Api.Guardian,
	issuer: "api",
	secret_key: "/Su4Cc5fzdybUNRA5bepVJKMtxKXXoHuAY/V62hc/0u9SnpcqL4IZHi5aJoPVM1D"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
